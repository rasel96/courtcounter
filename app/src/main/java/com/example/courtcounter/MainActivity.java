package com.example.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreTeamA = 0;
    int scoreTeamB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    // Team A is here!!
    public void add3onA(View v){
        scoreTeamA += 3;
        displayTeamA(scoreTeamA);
    }

    public void add2onA(View v){
        scoreTeamA += 2;
        displayTeamA(scoreTeamA);
    }

    public void add1onA(View v){
        scoreTeamA += 1;
        displayTeamA(scoreTeamA);
    }

    private void displayTeamA(int score){
        TextView temp = findViewById(R.id.scoreTeamA);
        temp.setText("" + score);
    }
    // Team A going down!


    // Team B goes here
    public void add3onB(View v){
        scoreTeamB += 3;
        displayTeamB(scoreTeamB);
    }

    public void add2onB(View v){
        scoreTeamB += 2;
        displayTeamB(scoreTeamB);
    }

    public void add1onB(View v){
        scoreTeamB += 1;
        displayTeamB(scoreTeamB);
    }

    private void displayTeamB(int score){
        TextView temp = findViewById(R.id.scoreTeamB);
        temp.setText("" + score);
    }
    // Team B going Down!!

    // Need a reset?
    public void resetScore(View v){
        scoreTeamA = 0;
        scoreTeamB = 0;
        displayTeamA(scoreTeamA);
        displayTeamB(scoreTeamB);
    }
}
